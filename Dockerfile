FROM openjdk:11
ARG jarFile
ENV MONGO_HOST="localhost:27017" KAFKA_BROKER="localhost:9092" SERVER_PORT="9010"
RUN mkdir /app
COPY ${jarFile} /app/pauta-service.jar
WORKDIR /app
EXPOSE 9010
ENTRYPOINT ["java", "-jar", "pauta-service.jar", "--spring.data.mongodb.host=${MONGO_HOST}", "--pauta.cloud.kafka.binder.brokers=${KAFKA_BROKER}", "--server.port=${SERVER_PORT}"]