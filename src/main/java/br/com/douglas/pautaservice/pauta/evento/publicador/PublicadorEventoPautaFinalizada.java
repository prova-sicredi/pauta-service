package br.com.douglas.pautaservice.pauta.evento.publicador;

import br.com.douglas.pautaservice.app.config.PropriedadesProdutorKafka;
import br.com.douglas.pautaservice.pauta.evento.model.PautaFinalizada;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Slf4j
public class PublicadorEventoPautaFinalizada {

    private static final String LOG_PUBLICACAO = "Enviado Evento de pauta finalizada com sucesso topico={}, offset={}, partition={}}: {}.";
    private static final String LOG_ERRO_PUBLICACAO = "Erro ao enviar Evento de pauta finalizada com sucesso para tópico=%s: %s.";

    private final PropriedadesProdutorKafka propriedadesProdutorKafka;
    private final KafkaTemplate<String, PautaFinalizada> kafkaTemplatePautaFinalizada;

    public void publicar(Pauta pauta) {
        PautaFinalizada pautaFinalizada = new PautaFinalizada(pauta.getUuid());
        kafkaTemplatePautaFinalizada.send(propriedadesProdutorKafka.getTopicoPautaFinalizada(),
                pauta.getUuid(),
                pautaFinalizada).addCallback(
                callbackEnvioComSucesso(pautaFinalizada),
                callbackErroEnvio(pautaFinalizada)
        );
    }

    private SuccessCallback<SendResult<String, PautaFinalizada>> callbackEnvioComSucesso(PautaFinalizada evento) {
        return result -> log.debug(
                LOG_PUBLICACAO,
                propriedadesProdutorKafka.getTopicoPautaFinalizada(),
                result.getRecordMetadata().offset(),
                evento.getUuidPauta(),
                evento
        );
    }

    private FailureCallback callbackErroEnvio(PautaFinalizada evento) {
        return error -> log.error(
                format(
                        LOG_ERRO_PUBLICACAO,
                        propriedadesProdutorKafka.getTopicoPautaFinalizada(),
                        evento
                ),
                error
        );
    }

}
