package br.com.douglas.pautaservice.pauta.evento.config;

import br.com.douglas.pautaservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.pautaservice.app.config.PropriedadesKafka;
import br.com.douglas.pautaservice.pauta.evento.model.PautaAberta;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class ProdutorEventoPautaAberta extends AbstractKafkaConfiguration {

    public ProdutorEventoPautaAberta(PropriedadesKafka propriedadesKafka) {
        super(propriedadesKafka);
    }

    @Bean
    public KafkaTemplate<String, PautaAberta> kafkaTemplatePautaAberta() {
        return new KafkaTemplate<>(producerFactory());
    }

    private ProducerFactory<String, PautaAberta> producerFactory() {
        return new DefaultKafkaProducerFactory<>(configuracoesProdutor());
    }

}
