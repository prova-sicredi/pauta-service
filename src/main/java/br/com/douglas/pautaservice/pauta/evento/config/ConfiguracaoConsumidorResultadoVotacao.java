package br.com.douglas.pautaservice.pauta.evento.config;

import br.com.douglas.pautaservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.pautaservice.app.config.PropriedadesConsumidor;
import br.com.douglas.pautaservice.app.config.PropriedadesKafka;
import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ErrorHandler;
import org.springframework.kafka.support.SendResult;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import static br.com.douglas.pautaservice.pauta.constants.ChavesPropriedadesEventosKafka.EVENTO_RESULTADO_VOTACAO_CONSUMER_FACTORY;
import static br.com.douglas.pautaservice.pauta.constants.ChavesPropriedadesEventosKafka.EVENTO_RESULTADO_VOTACAO_KAFKA_LISTENER_CONTAINER_FACTORY;

@Configuration
@Slf4j
public class ConfiguracaoConsumidorResultadoVotacao extends AbstractKafkaConfiguration {

    private final KafkaTemplate<String, ResultadoVotacao> kafkaTemplateResultadoVotacao;

    public ConfiguracaoConsumidorResultadoVotacao(PropriedadesKafka propriedadesKafka,
                                                  KafkaTemplate<String, ResultadoVotacao> kafkaTemplateResultadoVotacao) {
        super(propriedadesKafka);
        this.kafkaTemplateResultadoVotacao = kafkaTemplateResultadoVotacao;
    }

    @Bean(EVENTO_RESULTADO_VOTACAO_KAFKA_LISTENER_CONTAINER_FACTORY)
    public ConcurrentKafkaListenerContainerFactory<String, ResultadoVotacao> eventoSessaoVotacaoAbertaListener(
            ConsumerFactory<String, ResultadoVotacao> consumerFactory) {
        var factory = new ConcurrentKafkaListenerContainerFactory<String, ResultadoVotacao>();
        factory.setConsumerFactory(consumerFactory);
        factory.setErrorHandler(resultadoVotacaoErrorHandler());
        return factory;
    }

    @Bean(EVENTO_RESULTADO_VOTACAO_CONSUMER_FACTORY)
    public ConsumerFactory<String, ResultadoVotacao> eventoSessaoVotacaoAbertaConsumerFactory() {
        return new DefaultKafkaConsumerFactory<>(
                configuracoesConsumidor(),
                new StringDeserializer(),
                new JsonDeserializer<>(ResultadoVotacao.class)
        );
    }

    private ErrorHandler resultadoVotacaoErrorHandler() {
        return (exception, data) -> {
            PropriedadesConsumidor proriedadesConsumidor = propriedadesKafka.getProriedadesConsumidor();
            var record = (ConsumerRecord<String, ResultadoVotacao>) data;
            ResultadoVotacao resultadoVotacao = record.value();
            kafkaTemplateResultadoVotacao.send(proriedadesConsumidor.getTopicoResultadoVotacaoError(), record.key(), resultadoVotacao)
                    .addCallback(
                            callbackEnvioComSucesso(resultadoVotacao),
                            callbackErroEnvio(resultadoVotacao)
                    );
        };
    }

    private SuccessCallback<SendResult<String, ResultadoVotacao>> callbackEnvioComSucesso(ResultadoVotacao evento) {
        return result -> log.warn("Enviado SessaoVotacaoAberta para DLQ: {}.", evento);
    }

    private FailureCallback callbackErroEnvio(ResultadoVotacao evento) {
        return error -> log.error("Erro ao enviar SessaoVotacaoAberta para DLQ: " + evento, error);
    }

}
