package br.com.douglas.pautaservice.pauta.model.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@AllArgsConstructor
@ApiModel
@Validated
public class PautaRequest {

    @NotNull(message = "Nome da pauta é obrigatória.")
    @NotBlank(message = "Nome da pauta não pode estar vazio.")
    @ApiParam(required = true, example = "Voce sabe o que é Java?")
    private String nome;

    @ApiParam(example = "1")
    private Integer prazo;
}
