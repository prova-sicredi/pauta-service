package br.com.douglas.pautaservice.pauta.repository.mapper;

import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.repository.entity.PautaEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import reactor.core.publisher.Mono;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PautaEntityMapper {

    public static Mono<Pauta> toPauta(PautaEntity pautaEntity) {
        return Mono.just(Pauta.builder()
                .uuid(pautaEntity.getUuid())
                .nome(pautaEntity.getNome())
                .prazo(pautaEntity.getPrazo())
                .horarioAbertura(pautaEntity.getHorarioAbertura())
                .horarioFechamento(pautaEntity.getHorarioFechamento())
                .situacao(pautaEntity.getSituacao())
                .resultado(pautaEntity.getResultado())
                .resumoVotacao(pautaEntity.getResumoVotacao())
                .build());
    }

    public static Mono<PautaEntity> toPautaEntity(Pauta pauta) {
        return Mono.just(PautaEntity.builder()
                .uuid(pauta.getUuid())
                .nome(pauta.getNome())
                .horarioAbertura(pauta.getHorarioAbertura())
                .horarioFechamento(pauta.getHorarioFechamento())
                .prazo(pauta.getPrazo())
                .situacao(pauta.getSituacao())
                .resultado(pauta.getResultado())
                .resumoVotacao(pauta.getResumoVotacao())
                .build());
    }

}
