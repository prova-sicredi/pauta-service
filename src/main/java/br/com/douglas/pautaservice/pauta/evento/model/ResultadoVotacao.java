package br.com.douglas.pautaservice.pauta.evento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResultadoVotacao {
    private String uuidSessao;
    private String uuidPauta;
    private String resultado;
    private String resumoVotacao;
}
