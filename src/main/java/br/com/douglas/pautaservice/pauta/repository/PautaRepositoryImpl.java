package br.com.douglas.pautaservice.pauta.repository;

import br.com.douglas.pautaservice.app.exception.model.GenericException;
import br.com.douglas.pautaservice.app.exception.model.ResponseError;
import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.repository.mapper.PautaEntityMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
public class PautaRepositoryImpl implements PautaRepository {

    private static final String MENSAGEM_SALVAR = "Não foi possível salvar a pauta %s";

    private final PautaMongoRepository pautaMongoRepository;

    @Override
    public Mono<Pauta> salvar(Pauta pauta) {
        return PautaEntityMapper.toPautaEntity(pauta)
                .flatMap(pautaMongoRepository::save)
                .switchIfEmpty(Mono.error(buildError(format(MENSAGEM_SALVAR, pauta.getNome()),
                        format(MENSAGEM_SALVAR, pauta.getNome()), HttpStatus.INTERNAL_SERVER_ERROR)))
                .flatMap(PautaEntityMapper::toPauta);
    }

    @Override
    public Mono<Pauta> buscarPorUuid(String uuid) {
        return pautaMongoRepository.findById(uuid)
                .switchIfEmpty(Mono.error(buildError(format("Nenhuma pauta encontrada com o uuid %s", uuid),
                        "Verifique o uuid informado e tente novamente", HttpStatus.NOT_FOUND)))
                .flatMap(PautaEntityMapper::toPauta);
    }

    @Override
    public Flux<Pauta> buscarPautasAbertas() {
        return pautaMongoRepository.findAllBySituacao(SituacaoPauta.ABERTO)
                .flatMap(PautaEntityMapper::toPauta);
    }

    @Override
    public Mono<Pauta> fecharPauta(Pauta pauta) {
        return PautaEntityMapper.toPautaEntity(pauta)
                .doOnSuccess(pautaEntity -> pautaEntity.setSituacao(SituacaoPauta.FECHADO))
                .flatMap(pautaMongoRepository::save)
                .flatMap(PautaEntityMapper::toPauta);
    }

    private GenericException buildError(String mensagem, String mensagemUsuario, HttpStatus status) {
        return new GenericException(ResponseError.builder()
                .data(LocalDateTime.now())
                .mensagem(mensagem)
                .status(status)
                .mensagemUsuario(mensagemUsuario)
                .build());
    }
}
