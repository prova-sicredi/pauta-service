package br.com.douglas.pautaservice.pauta.evento.publicador;

import br.com.douglas.pautaservice.app.config.PropriedadesProdutorKafka;
import br.com.douglas.pautaservice.pauta.evento.model.PautaAberta;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Slf4j
public class PublicadorEventoPautaAberta {

    private static final String LOG_PUBLICACAO = "Enviado Evento de pauta aberta com sucesso topico={}, offset={}, partition={}}: {}.";
    private static final String LOG_ERRO_PUBLICACAO = "Erro ao enviar Evento de pauta aberta para tópico=%s: %s.";

    private final PropriedadesProdutorKafka propriedadesProdutorKafka;
    private final KafkaTemplate<String, PautaAberta> kafkaTemplatePautaAberta;

    public void publicar(Pauta pauta) {
        PautaAberta pautaAberta = new PautaAberta(pauta.getUuid(), pauta.getHorarioFechamento());
        kafkaTemplatePautaAberta.send(propriedadesProdutorKafka.getTopicoPautaAberta(),
                pautaAberta.getUuidPauta(),
                pautaAberta).addCallback(
                callbackEnvioComSucesso(pautaAberta),
                callbackErroEnvio(pautaAberta)
        );
    }

    private SuccessCallback<SendResult<String, PautaAberta>> callbackEnvioComSucesso(PautaAberta evento) {
        return result -> log.debug(
                LOG_PUBLICACAO,
                propriedadesProdutorKafka.getTopicoPautaFinalizada(),
                result.getRecordMetadata().offset(),
                evento.getUuidPauta(),
                evento
        );
    }

    private FailureCallback callbackErroEnvio(PautaAberta evento) {
        return error -> log.error(
                format(
                        LOG_ERRO_PUBLICACAO,
                        propriedadesProdutorKafka.getTopicoPautaFinalizada(),
                        evento
                ),
                error
        );
    }
}
