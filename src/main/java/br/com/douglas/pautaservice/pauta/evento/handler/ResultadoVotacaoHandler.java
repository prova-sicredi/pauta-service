package br.com.douglas.pautaservice.pauta.evento.handler;

import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface ResultadoVotacaoHandler {
    Mono<Pauta> atualizarResultadoVotacao(ResultadoVotacao resultadoVotacao);
}
