package br.com.douglas.pautaservice.pauta.evento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class PautaAberta {
    private String uuidPauta;
    private LocalDateTime horarioFechamento;
}
