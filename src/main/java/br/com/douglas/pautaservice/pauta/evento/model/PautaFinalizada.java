package br.com.douglas.pautaservice.pauta.evento.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class PautaFinalizada {
    private String uuidPauta;
}
