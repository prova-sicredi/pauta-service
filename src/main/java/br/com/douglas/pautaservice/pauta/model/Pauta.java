package br.com.douglas.pautaservice.pauta.model;

import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pauta {
    private String uuid;
    private String nome;
    private Integer prazo;
    private LocalDateTime horarioAbertura;
    private LocalDateTime horarioFechamento;
    private String resultado;
    private String resumoVotacao;
    private SituacaoPauta situacao;
}
