package br.com.douglas.pautaservice.pauta.evento.config;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.listener.ConsumerRecordRecoverer;

import java.util.function.BiConsumer;

public class CustomConsumerRecordRecoverer implements ConsumerRecordRecoverer {

    @Override
    public void accept(ConsumerRecord<?, ?> consumerRecord, Exception exception) {

    }

    @Override
    public BiConsumer<ConsumerRecord<?, ?>, Exception> andThen(BiConsumer<? super ConsumerRecord<?, ?>, ? super Exception> after) {
        return null;
    }
}
