package br.com.douglas.pautaservice.pauta.controller.v1;

import br.com.douglas.pautaservice.app.exception.model.ResponseError;
import br.com.douglas.pautaservice.pauta.model.request.PautaRequest;
import br.com.douglas.pautaservice.pauta.model.response.PautaResponse;
import br.com.douglas.pautaservice.pauta.service.PautaService;
import br.com.douglas.pautaservice.pauta.service.PautaServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import static br.com.douglas.pautaservice.app.constants.SwaggerTags.PAUTA_V1;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/v1/pautas")
@Api(tags = {PAUTA_V1}, produces = MediaType.APPLICATION_JSON_VALUE)
public class PautaController {

    private final PautaService pautaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Gerar uma nova pauta")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Pauta gerada com sucesso", response = String.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro interno do servidor", response = ResponseError.class)
    })
    public Mono<String> gerarUmaNovaPauta(@Valid @RequestBody Mono<PautaRequest> pautaRequest) {
        return pautaRequest.flatMap(pautaService::gerarNovaPauta);
    }

    @GetMapping("/{uuidPauta}")
    @ApiOperation(value = "Buscar os dados de uma pauta")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Pauta encontrada", response = PautaResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ResponseError.class),
            @ApiResponse(code = 404, message = "Pauta não encontrada", response = ResponseError.class),
            @ApiResponse(code = 500, message = "Erro interno do servidor", response = ResponseError.class)
    })
    public Mono<PautaResponse> buscarPauta(@PathVariable String uuidPauta) {
        return pautaService.buscarPauta(uuidPauta);
    }

}
