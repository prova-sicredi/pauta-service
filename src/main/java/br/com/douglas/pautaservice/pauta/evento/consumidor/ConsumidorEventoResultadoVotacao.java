package br.com.douglas.pautaservice.pauta.evento.consumidor;

import br.com.douglas.pautaservice.pauta.evento.handler.ResultadoVotacaoHandler;
import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import static br.com.douglas.pautaservice.app.constants.ChavesConfiguracoes.APPLICATION_ID_CONFIG;
import static br.com.douglas.pautaservice.app.constants.ChavesConfiguracoes.TOPICO_RESULTADO_VOTACAO;
import static br.com.douglas.pautaservice.pauta.constants.ChavesPropriedadesEventosKafka.EVENTO_RESULTADO_VOTACAO_KAFKA_LISTENER_CONTAINER_FACTORY;
import static java.lang.String.format;

@Component
@RequiredArgsConstructor
@Slf4j
public class ConsumidorEventoResultadoVotacao {

    private static final String LOG_PAUTA_ATUALIZADA = "Pauta %s atualizada com o resultado %s";
    public static final String LOG_ERRO = "Erro ao consumir mensagem de resultado finalizado: %s";

    private final ResultadoVotacaoHandler resultadoVotacaoHandler;

    @KafkaListener(
            topics = TOPICO_RESULTADO_VOTACAO,
            groupId = APPLICATION_ID_CONFIG,
            containerFactory = EVENTO_RESULTADO_VOTACAO_KAFKA_LISTENER_CONTAINER_FACTORY
    )
    public void listen(@Payload ResultadoVotacao resultadoVotacao) {
        resultadoVotacaoHandler.atualizarResultadoVotacao(resultadoVotacao)
                .doOnSuccess(pauta -> log.info(format(LOG_PAUTA_ATUALIZADA, pauta.getUuid(), pauta.getResultado())))
                .doOnError(error -> log.error(format(LOG_ERRO, error.getMessage())))
                .subscribe();
    }
}
