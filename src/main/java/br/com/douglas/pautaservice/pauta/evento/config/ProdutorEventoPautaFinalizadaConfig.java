package br.com.douglas.pautaservice.pauta.evento.config;

import br.com.douglas.pautaservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.pautaservice.app.config.PropriedadesKafka;
import br.com.douglas.pautaservice.pauta.evento.model.PautaFinalizada;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class ProdutorEventoPautaFinalizadaConfig extends AbstractKafkaConfiguration {

    public ProdutorEventoPautaFinalizadaConfig(PropriedadesKafka propriedadesKafka) {
        super(propriedadesKafka);
    }

    @Bean
    public KafkaTemplate<String, PautaFinalizada> kafkaTemplatePautaFinalizada() {
        return new KafkaTemplate<>(producerFactory());
    }

    private ProducerFactory<String, PautaFinalizada> producerFactory() {
        return new DefaultKafkaProducerFactory<>(configuracoesProdutor());
    }
}
