package br.com.douglas.pautaservice.pauta.mapper;

import br.com.douglas.pautaservice.app.provider.DateTimeProvider;
import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.model.request.PautaRequest;
import br.com.douglas.pautaservice.pauta.model.response.PautaResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

import static br.com.douglas.pautaservice.pauta.constants.PautaConstants.PRAZO_DEFAULT;

@Component
@RequiredArgsConstructor
public class PautaMapper {

    private final DateTimeProvider dateTimeProvider;

    public Mono<Pauta> toPauta(PautaRequest pautaRequest) {
        Integer prazo = resolvePrazo(pautaRequest.getPrazo());
        LocalDateTime horarioAbertura = dateTimeProvider.now();
        LocalDateTime horarioFechamento = horarioAbertura.plusMinutes(prazo);
        return Mono.just(Pauta.builder()
                .nome(pautaRequest.getNome())
                .prazo(resolvePrazo(prazo))
                .horarioAbertura(horarioAbertura)
                .horarioFechamento(horarioFechamento)
                .situacao(SituacaoPauta.ABERTO)
                .build());
    }

    public Mono<PautaResponse> toPautaResponse(Pauta pauta) {
        return Mono.just(PautaResponse.builder()
                .nome(pauta.getNome())
                .uuid(pauta.getUuid())
                .horarioAbertura(pauta.getHorarioAbertura())
                .horarioFechamento(pauta.getHorarioFechamento())
                .situacao(pauta.getSituacao())
                .resultado(pauta.getResultado())
                .resumoVotacao(pauta.getResumoVotacao())
                .build());
    }

    private static Integer resolvePrazo(Integer prazo) {
        return ObjectUtils.isEmpty(prazo) || prazo == 0 ? PRAZO_DEFAULT : prazo;
    }
}
