package br.com.douglas.pautaservice.pauta.repository.entity;

import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "pauta")
public class PautaEntity {

    @Id
    private String uuid;
    private String nome;
    private Integer prazo;
    private SituacaoPauta situacao;
    private LocalDateTime horarioAbertura;
    private LocalDateTime horarioFechamento;
    private String resultado;
    private String resumoVotacao;
}
