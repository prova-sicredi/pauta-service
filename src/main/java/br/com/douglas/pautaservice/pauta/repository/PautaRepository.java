package br.com.douglas.pautaservice.pauta.repository;

import br.com.douglas.pautaservice.pauta.model.Pauta;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PautaRepository {
    Mono<Pauta> salvar(Pauta pauta);
    Mono<Pauta> buscarPorUuid(String uuid);
    Flux<Pauta> buscarPautasAbertas();
    Mono<Pauta> fecharPauta(Pauta pauta);
}
