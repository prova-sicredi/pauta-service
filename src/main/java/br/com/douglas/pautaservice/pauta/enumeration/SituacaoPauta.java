package br.com.douglas.pautaservice.pauta.enumeration;

public enum SituacaoPauta {
    ABERTO,
    FECHADO,
    APURADO
}
