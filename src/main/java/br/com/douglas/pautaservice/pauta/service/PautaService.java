package br.com.douglas.pautaservice.pauta.service;

import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.model.request.PautaRequest;
import br.com.douglas.pautaservice.pauta.model.response.PautaResponse;
import reactor.core.publisher.Mono;

public interface PautaService {
    Mono<Void> validarPautasAbertasAoIniciar();
    Mono<String> gerarNovaPauta(PautaRequest pautaRequest);
    Mono<PautaResponse> buscarPauta(String uuidPauta);
    Mono<Pauta> fecharPauta(Pauta pauta);
    Mono<Pauta> atualizarResultado(ResultadoVotacao resultadoVotacao);
}
