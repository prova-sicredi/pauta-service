package br.com.douglas.pautaservice.pauta.service;

import br.com.douglas.pautaservice.app.provider.DateTimeProvider;
import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;
import br.com.douglas.pautaservice.pauta.evento.publicador.PublicadorEventoPautaAberta;
import br.com.douglas.pautaservice.pauta.evento.publicador.PublicadorEventoPautaFinalizada;
import br.com.douglas.pautaservice.pauta.mapper.PautaMapper;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.model.request.PautaRequest;
import br.com.douglas.pautaservice.pauta.model.response.PautaResponse;
import br.com.douglas.pautaservice.pauta.repository.PautaRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static br.com.douglas.pautaservice.pauta.constants.PautaCacheConstants.PAUTA_CACHE;

@Service
@Slf4j
@RequiredArgsConstructor
public class PautaServiceImpl implements PautaService {

    private final PautaRepository pautaRepository;
    private final PautaMapper pautaMapper;
    private final DateTimeProvider dateTimeProvider;
    private final PublicadorEventoPautaAberta publicadorEventoPautaAberta;
    private final PublicadorEventoPautaFinalizada publicadorEventoPautaFinalizada;

    @EventListener(value = ApplicationReadyEvent.class)
    @Override
    public Mono<Void> validarPautasAbertasAoIniciar() {
        return pautaRepository.buscarPautasAbertas()
                .flatMap(this::tratarFechamentoDasPautas)
                .doOnComplete(() -> log.info("Finalizado processo de validação ao inciar a aplicação"))
                .then();
    }

    @Override
    public Mono<String> gerarNovaPauta(PautaRequest pautaRequest) {
        return pautaMapper.toPauta(pautaRequest)
                .flatMap(pautaRepository::salvar)
                .doOnSuccess(publicadorEventoPautaAberta::publicar)
                .doOnSuccess(this::criaSchedule)
                .map(Pauta::getUuid);
    }

    @Cacheable(cacheNames = PAUTA_CACHE)
    @Override
    public Mono<PautaResponse> buscarPauta(String uuidPauta) {
        return pautaRepository.buscarPorUuid(uuidPauta)
                .flatMap(pautaMapper::toPautaResponse);
    }

    @CacheEvict(cacheNames = PAUTA_CACHE)
    @Override
    public Mono<Pauta> fecharPauta(Pauta pauta) {
        return pautaRepository.fecharPauta(pauta)
                .doOnSuccess(publicadorEventoPautaFinalizada::publicar);
    }

    @Override
    public Mono<Pauta> atualizarResultado(ResultadoVotacao resultadoVotacao) {
        return pautaRepository.buscarPorUuid(resultadoVotacao.getUuidPauta())
                .doOnSuccess(pauta -> pauta.setResultado(resultadoVotacao.getResultado()))
                .doOnSuccess(pauta -> pauta.setResumoVotacao(resultadoVotacao.getResumoVotacao()))
                .doOnSuccess(pauta -> pauta.setSituacao(SituacaoPauta.APURADO))
                .flatMap(pautaRepository::salvar);
    }

    private Flux<Pauta> tratarFechamentoDasPautas(Pauta pauta) {
        return Flux.just(pauta)
                .filter(this::jaPassouOPrazo)
                .flatMap(this::fecharPauta)
                .switchIfEmpty(Flux.defer(() -> Flux.just(criaSchedule(pauta))));
    }

    private boolean jaPassouOPrazo(Pauta pauta) {
        return pauta.getHorarioFechamento().isBefore(dateTimeProvider.now());
    }

    private Pauta criaSchedule(Pauta pauta) {
        long prazo = ChronoUnit.SECONDS.between(dateTimeProvider.now(), pauta.getHorarioFechamento());
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.schedule(() -> fecharPauta(pauta).subscribe(), prazo, TimeUnit.SECONDS);
        log.debug(String.format("Criado schedule da pauta %s para daqui a %d segundos (%d minutos)", pauta.getUuid(), prazo, prazo /60 ));
        return pauta;
    }

}
