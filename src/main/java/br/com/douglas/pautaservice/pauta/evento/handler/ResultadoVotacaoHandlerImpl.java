package br.com.douglas.pautaservice.pauta.evento.handler;

import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.service.PautaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class ResultadoVotacaoHandlerImpl implements ResultadoVotacaoHandler {

    private final PautaService pautaService;

    @Override
    public Mono<Pauta> atualizarResultadoVotacao(ResultadoVotacao resultadoVotacao) {
        return pautaService.atualizarResultado(resultadoVotacao);
    }
}
