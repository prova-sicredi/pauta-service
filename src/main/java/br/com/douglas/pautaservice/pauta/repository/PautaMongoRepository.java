package br.com.douglas.pautaservice.pauta.repository;

import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import br.com.douglas.pautaservice.pauta.repository.entity.PautaEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface PautaMongoRepository extends ReactiveMongoRepository<PautaEntity, String> {

    Flux<PautaEntity> findAllBySituacao(SituacaoPauta situacao);
}
