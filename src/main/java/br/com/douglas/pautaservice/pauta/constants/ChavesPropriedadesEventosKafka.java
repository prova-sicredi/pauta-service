package br.com.douglas.pautaservice.pauta.constants;

public interface ChavesPropriedadesEventosKafka {

    String EVENTO_RESULTADO_VOTACAO_CONSUMER_FACTORY = "eventoResultadoVotacaoConsumerFactory";
    String EVENTO_RESULTADO_VOTACAO_KAFKA_LISTENER_CONTAINER_FACTORY = "eventoResultadoVotacaoKafkaListenerContainerFactory";

}
