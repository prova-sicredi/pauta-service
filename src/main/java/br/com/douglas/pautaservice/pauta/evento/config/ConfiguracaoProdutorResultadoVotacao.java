package br.com.douglas.pautaservice.pauta.evento.config;

import br.com.douglas.pautaservice.app.config.AbstractKafkaConfiguration;
import br.com.douglas.pautaservice.app.config.PropriedadesKafka;
import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@Configuration
public class ConfiguracaoProdutorResultadoVotacao extends AbstractKafkaConfiguration {

    public ConfiguracaoProdutorResultadoVotacao(PropriedadesKafka propriedadesKafka) {
        super(propriedadesKafka);
    }

    @Bean
    public KafkaTemplate<String, ResultadoVotacao> kafkaTemplateResultadoVotacao() {
        return new KafkaTemplate<>(resultadoVotacaoKafkaProducerFactory());
    }

    public ProducerFactory<String, ResultadoVotacao> resultadoVotacaoKafkaProducerFactory() {
        return new DefaultKafkaProducerFactory<>(configuracoesProdutor());
    }

}
