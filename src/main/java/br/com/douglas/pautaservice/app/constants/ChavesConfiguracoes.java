package br.com.douglas.pautaservice.app.constants;

public interface ChavesConfiguracoes {
    String BROKERS = "${pauta.cloud.kafka.binder.brokers}";
    String ISOLATION_LEVEL = "${pauta.cloud.kafka.isolation-level:read_committed}";
    String AUTO_COMMIT = "${pauta.cloud.kafka.auto-commit:false}";
    String APPLICATION_ID_CONFIG = "${pauta.cloud.kafka.application-id:pauta-service}";
    String FETCH_MAX_WAIT_MS_CONFIG = "${pauta.cloud.kafka.fetch.max.wait.ms:}";
    String AUTO_OFFSET_RESET_CONFIG = "${pauta.cloud.kafka.auto.offset.reset:}";
    String ACKS = "${pauta.cloud.kafka.acks}";
    String COMPRESSION_TYPE = "${pauta.cloud.kafka.compression-type}";
    String ENABLE_IDEMPOTENCE = "${pauta.cloud.kafka.enable-idempotence}";
    String LINGER_MS = "${pauta.cloud.kafka.linger-ms}";
    String REQUEST_TIMEOUT_MS = "${pauta.cloud.kafka.request-timeout-ms}";
    String RETRIES = "${pauta.cloud.kafka.retries}";
    String RETRY_BACKOFF_MS = "${pauta.cloud.kafka.retry-backoff-ms}";
    String TRANSACTION_ID_SUFIX = "${pauta.cloud.kafka.transaction-id-sufix}";
    String TOPICO_PAUTA_FINALIZADA = "${pauta.cloud.kafka.topics.pauta-finalizada.topico}";
    String TOPICO_PAUTA_ABERTA = "${pauta.cloud.kafka.topics.pauta-aberta.topico}";
    String TOPICO_RESULTADO_VOTACAO = "${pauta.cloud.kafka.topics.resultado-votacao.topico}";
    String TOPICO_RESULTADO_VOTACAO_ERROR = "${pauta.cloud.kafka.topics.resultado-votacao.topico-erro}";
}
