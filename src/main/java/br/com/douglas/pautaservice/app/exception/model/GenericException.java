package br.com.douglas.pautaservice.app.exception.model;

import lombok.*;

@Builder
@Getter
@Setter
@NoArgsConstructor
public class GenericException extends RuntimeException {
    private ResponseError responseError;

    public GenericException(ResponseError responseError) {
        super(responseError.getMensagem());
        this.responseError = responseError;
    }
}
