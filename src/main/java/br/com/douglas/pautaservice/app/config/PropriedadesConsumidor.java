package br.com.douglas.pautaservice.app.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.douglas.pautaservice.app.constants.ChavesConfiguracoes.*;

@Getter
@Component
public class PropriedadesConsumidor {

    @Value(RETRIES)
    private Integer numeroTentativas;

    @Value(REQUEST_TIMEOUT_MS)
    private Integer intervaloRetentativasMs;

    @Value(TOPICO_RESULTADO_VOTACAO)
    private String topicoResultadoVotacao;

    @Value(TOPICO_RESULTADO_VOTACAO_ERROR)
    private String topicoResultadoVotacaoError;

}
