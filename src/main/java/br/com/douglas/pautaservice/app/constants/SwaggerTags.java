package br.com.douglas.pautaservice.app.constants;

public interface SwaggerTags {
    String PAUTA_V1 = "pautaV1";
    String DESCRICAO_PAUTA_V1 = "V1 da API de pautas";

}
