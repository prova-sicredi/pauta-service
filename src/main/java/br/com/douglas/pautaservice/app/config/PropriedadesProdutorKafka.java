package br.com.douglas.pautaservice.app.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import static br.com.douglas.pautaservice.app.constants.ChavesConfiguracoes.*;

@Getter
@Component
public class PropriedadesProdutorKafka {

    @Value(ACKS)
    private String acks;

    @Value(COMPRESSION_TYPE)
    private String compressionType;

    @Value(ENABLE_IDEMPOTENCE)
    private boolean enableIdempotence;

    @Value(LINGER_MS)
    private Integer lingerMs;

    @Value(REQUEST_TIMEOUT_MS)
    private Integer requestTimeoutMs;

    @Value(RETRIES)
    private Integer retries;

    @Value(RETRY_BACKOFF_MS)
    private Integer retryBackOffMs;

    @Value(TRANSACTION_ID_SUFIX)
    private String transactionalIdSufix;

    @Value(TOPICO_PAUTA_FINALIZADA)
    private String topicoPautaFinalizada;

    @Value(TOPICO_PAUTA_ABERTA)
    private String topicoPautaAberta;

}