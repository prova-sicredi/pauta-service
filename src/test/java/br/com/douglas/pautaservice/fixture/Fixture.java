package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;

import java.time.LocalDateTime;

import static java.time.Month.NOVEMBER;

public interface Fixture {

    String UUID_SESSAO = "618dd2efb11ebb248bd97eae";
    String UUID_PAUTA = "618c7c2c57a3970db3edefa8";
    String UUID_PAUTA_2 = "618c7e3457a3970db3edefa9";
    String UUID_PAUTA_3 = "618c800c57a3970db3edefaa";
    String NOME_PAUTA = "Você sabe o que é Java?";
    SituacaoPauta SITUACAO_PAUTA = SituacaoPauta.ABERTO;
    Integer PRAZO_PAUTA = 5;
    String RESUTADO_VOTACAO = "SIM";
    String RESUMO_VOTACAO = "SIM: 2 | NAO: 0";
    String TOPICO_PAUTA_ABERTA = "topicoPautaAberta";
    String TOPICO_PAUTA_FINALIZADA = "topicoPautaFinalizada";

    LocalDateTime _11_11_2021_22_18_00 = LocalDateTime.of(2021, NOVEMBER, 11, 22,18);
    LocalDateTime _11_11_2021_22_23_00 = LocalDateTime.of(2021, NOVEMBER, 11, 22,23);
    LocalDateTime _11_11_2021_22_15_00 = LocalDateTime.of(2021, NOVEMBER, 11, 22,15);

}
