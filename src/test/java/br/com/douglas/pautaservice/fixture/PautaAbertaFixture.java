package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.evento.model.PautaAberta;

import static br.com.douglas.pautaservice.fixture.Fixture.UUID_PAUTA;
import static br.com.douglas.pautaservice.fixture.Fixture._11_11_2021_22_23_00;

public class PautaAbertaFixture {

    public static PautaAberta pautaAberta() {
        return PautaAberta.builder()
                .uuidPauta(UUID_PAUTA)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }
}
