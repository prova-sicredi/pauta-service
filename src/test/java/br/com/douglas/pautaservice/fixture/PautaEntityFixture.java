package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.repository.entity.PautaEntity;

import static br.com.douglas.pautaservice.fixture.Fixture.*;

public class PautaEntityFixture {

    public static PautaEntity pautaEntity() {
        return PautaEntity.builder()
                .uuid(UUID_PAUTA)
                .nome(NOME_PAUTA)
                .resumoVotacao(RESUMO_VOTACAO)
                .situacao(SITUACAO_PAUTA)
                .prazo(PRAZO_PAUTA)
                .resultado(RESUTADO_VOTACAO)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }

    public static PautaEntity pautaEntity(String uuidPauta) {
        return PautaEntity.builder()
                .uuid(uuidPauta)
                .nome(NOME_PAUTA)
                .resumoVotacao(RESUMO_VOTACAO)
                .situacao(SITUACAO_PAUTA)
                .prazo(PRAZO_PAUTA)
                .resultado(RESUTADO_VOTACAO)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }

}
