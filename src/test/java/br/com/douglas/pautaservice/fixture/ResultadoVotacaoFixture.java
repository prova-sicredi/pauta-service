package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.evento.model.ResultadoVotacao;

import static br.com.douglas.pautaservice.fixture.Fixture.*;

public class ResultadoVotacaoFixture {

    public static ResultadoVotacao resultadoVotacao() {
        return ResultadoVotacao.builder()
                .resultado(RESUTADO_VOTACAO)
                .resumoVotacao(RESUMO_VOTACAO)
                .uuidPauta(UUID_PAUTA)
                .uuidSessao(UUID_SESSAO)
                .build();
    }
}
