package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.evento.model.PautaFinalizada;

import static br.com.douglas.pautaservice.fixture.Fixture.UUID_PAUTA;

public class PautaFinalizadaFixture {

    public static PautaFinalizada pautaFinalizada() {
        return PautaFinalizada.builder()
                .uuidPauta(UUID_PAUTA)
                .build();
    }
}
