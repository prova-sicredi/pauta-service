package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.model.request.PautaRequest;

public class PautaRequestFixture {

    public static PautaRequest pautaRequest() {
        return PautaRequest.builder()
                .nome(Fixture.NOME_PAUTA)
                .prazo(Fixture.PRAZO_PAUTA)
                .build();
    }
}
