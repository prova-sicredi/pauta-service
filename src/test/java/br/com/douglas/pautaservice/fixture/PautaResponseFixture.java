package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.model.response.PautaResponse;

import static br.com.douglas.pautaservice.fixture.Fixture.*;
import static br.com.douglas.pautaservice.fixture.Fixture._11_11_2021_22_23_00;

public class PautaResponseFixture {

    public static PautaResponse pautaResponse() {
        return PautaResponse.builder()
                .uuid(UUID_PAUTA)
                .nome(NOME_PAUTA)
                .resumoVotacao(RESUMO_VOTACAO)
                .situacao(SITUACAO_PAUTA)
                .resultado(RESUTADO_VOTACAO)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }
}
