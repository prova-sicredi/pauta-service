package br.com.douglas.pautaservice.fixture;

import br.com.douglas.pautaservice.pauta.model.Pauta;

import java.time.LocalDateTime;

import static br.com.douglas.pautaservice.fixture.Fixture.*;

public class PautaFixture {

    public static Pauta pauta() {
        return Pauta.builder()
                .uuid(UUID_PAUTA)
                .nome(NOME_PAUTA)
                .resumoVotacao(RESUMO_VOTACAO)
                .situacao(SITUACAO_PAUTA)
                .resultado(RESUTADO_VOTACAO)
                .prazo(PRAZO_PAUTA)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }

    public static Pauta pauta(String uuidPauta) {
        return Pauta.builder()
                .uuid(uuidPauta)
                .nome(NOME_PAUTA)
                .prazo(PRAZO_PAUTA)
                .resumoVotacao(RESUMO_VOTACAO)
                .situacao(SITUACAO_PAUTA)
                .resultado(RESUTADO_VOTACAO)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }

    public static Pauta pauta(String uuidPauta, LocalDateTime horarioFechamento) {
        return Pauta.builder()
                .uuid(uuidPauta)
                .nome(NOME_PAUTA)
                .prazo(PRAZO_PAUTA)
                .resumoVotacao(RESUMO_VOTACAO)
                .situacao(SITUACAO_PAUTA)
                .resultado(RESUTADO_VOTACAO)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(horarioFechamento)
                .build();
    }

    public static Pauta pautaSemResultado() {
        return Pauta.builder()
                .uuid(UUID_PAUTA)
                .nome(NOME_PAUTA)
                .situacao(SITUACAO_PAUTA)
                .prazo(PRAZO_PAUTA)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }

    public static Pauta pautaNaoSalva() {
        return Pauta.builder()
                .nome(NOME_PAUTA)
                .situacao(SITUACAO_PAUTA)
                .prazo(PRAZO_PAUTA)
                .horarioAbertura(_11_11_2021_22_18_00)
                .horarioFechamento(_11_11_2021_22_23_00)
                .build();
    }
}
