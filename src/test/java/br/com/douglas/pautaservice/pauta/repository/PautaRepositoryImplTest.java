package br.com.douglas.pautaservice.pauta.repository;

import br.com.douglas.pautaservice.app.exception.model.GenericException;
import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.repository.entity.PautaEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static br.com.douglas.pautaservice.fixture.Fixture.*;
import static br.com.douglas.pautaservice.fixture.PautaEntityFixture.pautaEntity;
import static br.com.douglas.pautaservice.fixture.PautaFixture.pauta;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PautaRepositoryImplTest {

    @Mock
    private PautaMongoRepository pautaMongoRepository;

    @InjectMocks
    private PautaRepositoryImpl pautaRepository;

    @DisplayName("Dada uma instancia de Pauta, deve chamar o repository com pautaEntity correta")
    @Test
    void deveChamarOSalvarComAEntidadeCorreta() {
        doReturn(Mono.just(pautaEntity())).when(pautaMongoRepository).save(pautaEntity());
        StepVerifier.create(pautaRepository.salvar(pauta()))
                .expectNext(pauta())
                .verifyComplete();
        verify(pautaMongoRepository, times(1)).save(pautaEntity());
    }

    @DisplayName("Dado que o repository nao encontre nenhuma pauta, " +
            "deve retornar o erro com mensagem correta")
    @Test
    void deveRetornarUmErroCasoOMongoRepositoryRetorneUmMonoVazio() {
        doReturn(Mono.empty()).when(pautaMongoRepository).save(any());
        StepVerifier.create(pautaRepository.salvar(pauta()))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals("Não foi possível salvar a pauta Você sabe o que é Java?")
                        && ((GenericException) exception).getResponseError().getStatus().equals(HttpStatus.INTERNAL_SERVER_ERROR))
                .verify();
    }

    @DisplayName("Dado um UUID de pauta que exista na base, deve retornar a pauta corretamente")
    @Test
    void deveRetornarAPautaCorretamenteAoBuscar() {
        doReturn(Mono.just(pautaEntity())).when(pautaMongoRepository).findById(UUID_PAUTA);
        StepVerifier.create(pautaRepository.buscarPorUuid(UUID_PAUTA))
                .expectNext(pauta())
                .verifyComplete();
    }

    @DisplayName("Dado um UUID pauta que não exista na base, deve retornar uma " +
            "exception com a mensagem correta")
    @Test
    void deveRetornarUmErroSeOMongoRepositoryRetornarVazioAoBuscarPorUUID() {
        doReturn(Mono.empty()).when(pautaMongoRepository).findById(UUID_PAUTA);
        StepVerifier.create(pautaRepository.buscarPorUuid(UUID_PAUTA))
                .expectErrorMatches(exception -> exception instanceof GenericException
                        && exception.getMessage().equals("Nenhuma pauta encontrada com o uuid " + UUID_PAUTA)
                        && ((GenericException) exception).getResponseError().getStatus().equals(HttpStatus.NOT_FOUND))
                .verify();
    }

    @DisplayName("Dado que existam pautas abertas na base, deve retornar as pautas corretamente")
    @Test
    void deveRetornarTodasAsPautasAbertas() {
        doReturn(Flux.just(pautaEntity(), pautaEntity(UUID_PAUTA_2), pautaEntity(UUID_PAUTA_3)))
                .when(pautaMongoRepository).findAllBySituacao(SituacaoPauta.ABERTO);
        StepVerifier.create(pautaRepository.buscarPautasAbertas())
                .expectNext(pauta())
                .expectNext(pauta(UUID_PAUTA_2))
                .expectNext(pauta(UUID_PAUTA_3))
                .verifyComplete();
    }

    @DisplayName("Dada uma pauta, deve salvar ela corretamente com status FECHADO")
    @Test
    void deveSalvarAPautaComStatusFechado() {
        Pauta esperado = pauta();
        esperado.setSituacao(SituacaoPauta.FECHADO);
        PautaEntity entidade = pautaEntity();
        entidade.setSituacao(SituacaoPauta.FECHADO);
        doReturn(Mono.just(entidade)).when(pautaMongoRepository).save(entidade);
        StepVerifier.create(pautaRepository.fecharPauta(pauta()))
                .expectNext(esperado)
                .verifyComplete();
    }
}