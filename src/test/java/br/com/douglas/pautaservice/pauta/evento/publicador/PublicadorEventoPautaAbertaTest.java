package br.com.douglas.pautaservice.pauta.evento.publicador;

import br.com.douglas.pautaservice.app.config.PropriedadesProdutorKafka;
import br.com.douglas.pautaservice.fixture.Fixture;
import br.com.douglas.pautaservice.pauta.evento.model.PautaAberta;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.SettableListenableFuture;

import static br.com.douglas.pautaservice.fixture.Fixture.TOPICO_PAUTA_ABERTA;
import static br.com.douglas.pautaservice.fixture.Fixture.UUID_PAUTA;
import static br.com.douglas.pautaservice.fixture.PautaAbertaFixture.pautaAberta;
import static br.com.douglas.pautaservice.fixture.PautaFixture.pauta;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PublicadorEventoPautaAbertaTest {

    @Mock
    private KafkaTemplate<String, PautaAberta> kafkaTemplate;

    @Mock
    private PropriedadesProdutorKafka propriedadesProdutorKafka;

    @InjectMocks
    private PublicadorEventoPautaAberta publicadorEventoPautaAberta;

    @DisplayName("Dado uma pauta, deve chamar o kafkaTemplate de para enviar o evento com a PautaAberta correta")
    @Test
    void deveChamarOSendDoKafkaTemplate() {
        doReturn(TOPICO_PAUTA_ABERTA).when(propriedadesProdutorKafka).getTopicoPautaAberta();
        doReturn(new SettableListenableFuture<SendResult<String, PautaAberta>>())
                .when(kafkaTemplate).send(TOPICO_PAUTA_ABERTA, UUID_PAUTA, pautaAberta());
        publicadorEventoPautaAberta.publicar(pauta());
        verify(kafkaTemplate, times(1)).send(TOPICO_PAUTA_ABERTA, UUID_PAUTA, pautaAberta());
    }

}