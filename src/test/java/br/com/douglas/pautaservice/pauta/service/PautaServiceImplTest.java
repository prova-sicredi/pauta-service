package br.com.douglas.pautaservice.pauta.service;

import br.com.douglas.pautaservice.app.provider.DateTimeProvider;
import br.com.douglas.pautaservice.pauta.enumeration.SituacaoPauta;
import br.com.douglas.pautaservice.pauta.evento.publicador.PublicadorEventoPautaAberta;
import br.com.douglas.pautaservice.pauta.evento.publicador.PublicadorEventoPautaFinalizada;
import br.com.douglas.pautaservice.pauta.mapper.PautaMapper;
import br.com.douglas.pautaservice.pauta.model.Pauta;
import br.com.douglas.pautaservice.pauta.repository.PautaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static br.com.douglas.pautaservice.fixture.Fixture.*;
import static br.com.douglas.pautaservice.fixture.PautaFixture.pauta;
import static br.com.douglas.pautaservice.fixture.PautaFixture.pautaSemResultado;
import static br.com.douglas.pautaservice.fixture.PautaRequestFixture.pautaRequest;
import static br.com.douglas.pautaservice.fixture.PautaResponseFixture.pautaResponse;
import static br.com.douglas.pautaservice.fixture.ResultadoVotacaoFixture.resultadoVotacao;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PautaServiceImplTest {

    @Mock
    private PautaRepository pautaRepository;
    @Mock
    private PautaMapper pautaMapper;
    @Mock
    private DateTimeProvider dateTimeProvider;
    @Mock
    private PublicadorEventoPautaAberta publicadorEventoPautaAberta;
    @Mock
    private PublicadorEventoPautaFinalizada publicadorEventoPautaFinalizada;

    @InjectMocks
    private PautaServiceImpl pautaService;

    @BeforeEach
    void setup() {
        lenient().doReturn(_11_11_2021_22_18_00).when(dateTimeProvider).now();
    }

    @DisplayName("Dado que existam pautas abertas na base, deve chamar " +
            "o metodo de fechar apenas para as pautas que ja expiraram")
    @Test
    void deveRetornarUmMonoEmptyChamandoCorretamenteAQuantidadeDePautasFechadas() {
        Pauta pauta1 = pauta(UUID_PAUTA, _11_11_2021_22_15_00);
        Pauta pauta2 = pauta(UUID_PAUTA_2);
        Pauta pauta3 = pauta(UUID_PAUTA_3, _11_11_2021_22_15_00);

        doReturn(Mono.just(pauta1)).when(pautaRepository).fecharPauta(pauta1);
        doReturn(Mono.just(pauta3)).when(pautaRepository).fecharPauta(pauta3);
        doReturn(Flux.just(pauta1, pauta2, pauta3)).when(pautaRepository).buscarPautasAbertas();

        StepVerifier.create(pautaService.validarPautasAbertasAoIniciar())
                .verifyComplete();
        verify(pautaRepository, times(1)).fecharPauta(pauta1);
        verify(pautaRepository, never()).fecharPauta(pauta2);
        verify(pautaRepository, times(1)).fecharPauta(pauta3);
    }

    @DisplayName("Dada uma PautaRequest, deve retornar o UUID da pauta gerada")
    @Test
    void deveOUUIDDaPautaGeradaEChamarOPublicadorDoEvento() {
        doReturn(Mono.just(pauta())).when(pautaMapper).toPauta(pautaRequest());
        doReturn(Mono.just(pauta())).when(pautaRepository).salvar(pauta());
        StepVerifier.create(pautaService.gerarNovaPauta(pautaRequest()))
                .expectNext(UUID_PAUTA)
                .verifyComplete();
        verify(publicadorEventoPautaAberta, times(1)).publicar(pauta());
    }

    @DisplayName("Dado um UUID de um pauta que existe na base, deve retornar a pauta corretamente")
    @Test
    void deveRetornarAPautaCorretaQuandoBuscarPorUUID() {
        doReturn(Mono.just(pauta())).when(pautaRepository).buscarPorUuid(UUID_PAUTA);
        doReturn(Mono.just(pautaResponse())).when(pautaMapper).toPautaResponse(pauta());
        StepVerifier.create(pautaService.buscarPauta(UUID_PAUTA))
                .expectNext(pautaResponse())
                .verifyComplete();
    }

    @DisplayName("Dada uma Pauta, deve chamar o repository para fechar a pauta e " +
            "depois chamar o publicador de pauta finalizada")
    @Test
    void deveFecharAPautaEChamarOPublicar() {
        doReturn(Mono.just(pauta())).when(pautaRepository).fecharPauta(pauta());
        StepVerifier.create(pautaService.fecharPauta(pauta()))
                .expectNext(pauta())
                .verifyComplete();
        verify(publicadorEventoPautaFinalizada, times(1)).publicar(pauta());
    }

    @DisplayName("Dado um ResultadoVotacao, deve chamar o metodo de salvar e retornar a pauta corretamente")
    @Test
    void deveRetornarAPautaCorretaAoChamarAtualizarResutado() {
        Pauta esperado = pauta();
        esperado.setSituacao(SituacaoPauta.APURADO);
        doReturn(Mono.just(pautaSemResultado())).when(pautaRepository).buscarPorUuid(UUID_PAUTA);
        doReturn(Mono.just(esperado)).when(pautaRepository).salvar(esperado);
        StepVerifier.create(pautaService.atualizarResultado(resultadoVotacao()))
                .expectNext(esperado)
                .verifyComplete();
        verify(pautaRepository, times(1)).salvar(esperado);
    }

}