package br.com.douglas.pautaservice.pauta.mapper;

import br.com.douglas.pautaservice.app.provider.DateTimeProvider;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.test.StepVerifier;

import static br.com.douglas.pautaservice.fixture.Fixture._11_11_2021_22_18_00;
import static br.com.douglas.pautaservice.fixture.PautaFixture.pauta;
import static br.com.douglas.pautaservice.fixture.PautaFixture.pautaNaoSalva;
import static br.com.douglas.pautaservice.fixture.PautaRequestFixture.pautaRequest;
import static br.com.douglas.pautaservice.fixture.PautaResponseFixture.pautaResponse;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class PautaMapperTest {

    @Mock
    private DateTimeProvider dateTimeProvider;

    @InjectMocks
    private PautaMapper pautaMapper;

    @DisplayName("Dado uma PautaRequest, deve mapear corretamente para Pauta")
    @Test
    void deveMapearAPautaRequest() {
        doReturn(_11_11_2021_22_18_00).when(dateTimeProvider).now();
        StepVerifier.create(pautaMapper.toPauta(pautaRequest()))
                .expectNext(pautaNaoSalva())
                .verifyComplete();
    }

    @DisplayName("Dada uma Pauta, deve mapear corretamente para PautaResponse")
    @Test
    void deveMapearAPauta() {
        StepVerifier.create(pautaMapper.toPautaResponse(pauta()))
                .expectNext(pautaResponse())
                .verifyComplete();
    }

}