package br.com.douglas.pautaservice.pauta.evento.publicador;

import br.com.douglas.pautaservice.app.config.PropriedadesProdutorKafka;
import br.com.douglas.pautaservice.fixture.PautaFinalizadaFixture;
import br.com.douglas.pautaservice.pauta.evento.model.PautaFinalizada;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.SettableListenableFuture;

import static br.com.douglas.pautaservice.fixture.Fixture.*;
import static br.com.douglas.pautaservice.fixture.PautaFinalizadaFixture.pautaFinalizada;
import static br.com.douglas.pautaservice.fixture.PautaFixture.pauta;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PublicadorEventoPautaFinalizadaTest {

    @Mock
    private KafkaTemplate<String, PautaFinalizada> kafkaTemplate;

    @Mock
    private PropriedadesProdutorKafka propriedadesProdutorKafka;

    @InjectMocks
    private PublicadorEventoPautaFinalizada publicadorEventoPautaFinalizada;

    @DisplayName("Dada uma PautaFinalizada, deve chamar o send com a PautaFinalizada correta")
    @Test
    void deveChamarOSendDoKafkaTemplate() {
        doReturn(new SettableListenableFuture<SendResult<String, PautaFinalizadaFixture>>())
                .when(kafkaTemplate).send(TOPICO_PAUTA_FINALIZADA, UUID_PAUTA, pautaFinalizada());
        doReturn(TOPICO_PAUTA_FINALIZADA).when(propriedadesProdutorKafka).getTopicoPautaFinalizada();
        publicadorEventoPautaFinalizada.publicar(pauta());
        verify(kafkaTemplate, times(1)).send(TOPICO_PAUTA_FINALIZADA, UUID_PAUTA, pautaFinalizada());
    }
}