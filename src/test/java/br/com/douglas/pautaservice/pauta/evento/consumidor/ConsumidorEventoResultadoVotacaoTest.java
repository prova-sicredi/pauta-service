package br.com.douglas.pautaservice.pauta.evento.consumidor;

import br.com.douglas.pautaservice.pauta.evento.handler.ResultadoVotacaoHandler;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import static br.com.douglas.pautaservice.fixture.PautaFixture.pauta;
import static br.com.douglas.pautaservice.fixture.ResultadoVotacaoFixture.resultadoVotacao;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConsumidorEventoResultadoVotacaoTest {

    @Mock
    private ResultadoVotacaoHandler resultadoVotacaoHandler;

    @InjectMocks
    private ConsumidorEventoResultadoVotacao consumidorEventoResultadoVotacao;

    @DisplayName("Dado um evento de resultado votacao, deve chamar o handler")
    @Test
    void deveChamarOHandler() {
        doReturn(Mono.just(pauta())).when(resultadoVotacaoHandler).atualizarResultadoVotacao(resultadoVotacao());
        consumidorEventoResultadoVotacao.listen(resultadoVotacao());
        verify(resultadoVotacaoHandler, times(1)).atualizarResultadoVotacao(resultadoVotacao());
    }

}